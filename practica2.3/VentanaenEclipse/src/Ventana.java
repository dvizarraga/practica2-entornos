import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JComboBox;
import javax.swing.JTextPane;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import java.awt.Toolkit;
import java.awt.Color;

/**
 * 
 * @author daniel vizarraga
 * @since 13-12-2017
 *
 */
public class Ventana extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana frame = new Ventana();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ventana() {
		setTitle("SuperOrdenadores");
		setIconImage(Toolkit.getDefaultToolkit().getImage(Ventana.class.getResource("/imagen/superman.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 662, 433);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnMenu = new JMenu("menu");
		menuBar.add(mnMenu);
		
		JMenuItem mntmAbrirArchivo = new JMenuItem("Abrir archivo");
		mnMenu.add(mntmAbrirArchivo);
		
		JMenu mnAyuda = new JMenu("ayuda");
		menuBar.add(mnAyuda);
		
		JMenuItem mntmAcercaDe = new JMenuItem("acerca de");
		mnAyuda.add(mntmAcercaDe);
		
		JMenu mnInformacion = new JMenu("Informacion");
		menuBar.add(mnInformacion);
		
		JMenuItem mntmTienda = new JMenuItem("Tienda");
		mnInformacion.add(mntmTienda);
		contentPane = new JPanel();
		contentPane.setBackground(Color.ORANGE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBackground(Color.WHITE);
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Espa\u00F1ol", "Ingles"}));
		comboBox.setBounds(520, 14, 99, 22);
		contentPane.add(comboBox);
		
		JButton btnTraducir = new JButton("traducir");
		btnTraducir.setBackground(Color.WHITE);
		btnTraducir.setForeground(Color.WHITE);
		btnTraducir.setBounds(522, 49, 97, 25);
		contentPane.add(btnTraducir);
		
		JRadioButton rdbtnComprar = new JRadioButton("comprar");
		rdbtnComprar.setBackground(Color.WHITE);
		rdbtnComprar.setBounds(30, 49, 127, 25);
		contentPane.add(rdbtnComprar);
		
		JRadioButton rdbtnVender = new JRadioButton("vender");
		rdbtnVender.setBackground(Color.WHITE);
		rdbtnVender.setBounds(30, 79, 127, 25);
		contentPane.add(rdbtnVender);
		
		JLabel lblQueQuieresHacer = new JLabel("\u00BFQue quieres hacer?");
		lblQueQuieresHacer.setBounds(30, 17, 127, 16);
		contentPane.add(lblQueQuieresHacer);
		
		JLabel lblTamaoDeLa = new JLabel("Tama\u00F1o de la pantalla");
		lblTamaoDeLa.setBounds(208, 17, 133, 16);
		contentPane.add(lblTamaoDeLa);
		
		JRadioButton radioButton = new JRadioButton("15");
		radioButton.setBackground(Color.WHITE);
		radioButton.setBounds(208, 79, 127, 25);
		contentPane.add(radioButton);
		
		JRadioButton radioButton_1 = new JRadioButton("17");
		radioButton_1.setBackground(Color.WHITE);
		radioButton_1.setBounds(208, 112, 127, 25);
		contentPane.add(radioButton_1);
		
		JRadioButton radioButton_2 = new JRadioButton("13");
		radioButton_2.setBackground(Color.WHITE);
		radioButton_2.setBounds(208, 42, 127, 25);
		contentPane.add(radioButton_2);
		
		JLabel lblDireccion = new JLabel("Direccion");
		lblDireccion.setBounds(30, 191, 56, 16);
		contentPane.add(lblDireccion);
		
		JTextPane txtpnEscribeDireccion = new JTextPane();
		txtpnEscribeDireccion.setText("Escribe direccion");
		txtpnEscribeDireccion.setBounds(30, 220, 236, 22);
		contentPane.add(txtpnEscribeDireccion);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setBackground(Color.WHITE);
		comboBox_1.setModel(new DefaultComboBoxModel(new String[] {"Visa", "Paypal"}));
		comboBox_1.setBounds(30, 145, 88, 22);
		contentPane.add(comboBox_1);
		
		JLabel lblComoQuieresPagar = new JLabel("\u00BFComo quieres pagar?");
		lblComoQuieresPagar.setBounds(30, 121, 144, 16);
		contentPane.add(lblComoQuieresPagar);
		
		JButton btnContinuar = new JButton("Continuar");
		btnContinuar.setBackground(Color.WHITE);
		btnContinuar.setForeground(Color.WHITE);
		btnContinuar.setBounds(31, 310, 97, 25);
		contentPane.add(btnContinuar);
		
		JSlider slider = new JSlider();
		slider.setMajorTickSpacing(50);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		slider.setBounds(383, 264, 213, 45);
		contentPane.add(slider);
		
		JLabel lblquePlazoDe = new JLabel("\u00BFQue plazo de entrega quieres?");
		lblquePlazoDe.setBounds(386, 226, 210, 16);
		contentPane.add(lblquePlazoDe);
		
		JLabel lblDia = new JLabel("1 dia");
		lblDia.setBounds(393, 319, 56, 16);
		contentPane.add(lblDia);
		
		JLabel lblDias = new JLabel("7 dias");
		lblDias.setBounds(468, 319, 56, 16);
		contentPane.add(lblDias);
		
		JLabel lblDias_1 = new JLabel("10 dias");
		lblDias_1.setBounds(540, 319, 56, 16);
		contentPane.add(lblDias_1);
		
		JCheckBox chckbxquieresSerSocio = new JCheckBox("\u00BFQuieres ser socio?");
		chckbxquieresSerSocio.setBackground(Color.WHITE);
		chckbxquieresSerSocio.setBounds(30, 264, 150, 25);
		contentPane.add(chckbxquieresSerSocio);
		
		JLabel lbltipoDeEnvio = new JLabel("\u00BFTipo de envio?");
		lbltipoDeEnvio.setBounds(383, 17, 99, 16);
		contentPane.add(lbltipoDeEnvio);
		
		JComboBox comboBox_2 = new JComboBox();
		comboBox_2.setModel(new DefaultComboBoxModel(new String[] {"A domicilio", "Recoger tienda"}));
		comboBox_2.setBounds(383, 50, 99, 22);
		contentPane.add(comboBox_2);
		
		JSpinner spinner_1 = new JSpinner();
		spinner_1.setBounds(393, 155, 30, 22);
		contentPane.add(spinner_1);
		
		JLabel lblcuantosOrdenadoresQuieres = new JLabel("\u00BFCuantos ordenadores quieres?");
		lblcuantosOrdenadoresQuieres.setBounds(383, 116, 185, 16);
		contentPane.add(lblcuantosOrdenadoresQuieres);
	}
}
