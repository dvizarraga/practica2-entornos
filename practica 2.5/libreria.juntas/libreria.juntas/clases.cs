﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibreriaVS;


namespace libreria.juntas
{
    public class clases
    {
        static void Main(string[] args)
        {
            int num = 8;
            int n = 5;
            int x = 4, z = 2;
            string[] v;
            string cadena = "Pratica 2.5";
            char[] splitchar = { ' ' };
            v = cadena.Split(splitchar);
            
            Class1.esprimo(num);
            Class1.cfactorial(n);
            Class1.sumar(n);
            Class1.restar(n);
            Class1.multiplicar(n);


            Class2.cadena(cadena);
            Class2.piramide(n);
            Class2.cadenaSplit(cadena);
            Class2.visualizarVector(v);
            Class2.potencia(x, z);

        }
    } }
