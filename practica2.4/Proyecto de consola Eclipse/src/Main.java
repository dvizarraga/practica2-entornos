import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);

		int opcion, saldo = 500, n = 0, num,factorial;
		String respuesta;
		do{
			do {
			System.out.println("Elige una opcion (1-4)");
			
			System.out.println("1 Mirar tu saldo");
			System.out.println("2 Ingresar dinero");
			System.out.println("3 Comprobar numero primo");
			System.out.println("4 Obterner el factorial de un numero");
			opcion= in.nextInt();
			
			if (opcion > 4) {
				System.out.println("Error, pon un numero del 1 al 4 ");
			}
		} while (opcion > 4);

		switch (opcion) {
		case 1:if(opcion==1) {
			saldo = mirarsaldo(saldo);
		}
			break;
		case 2:if(opcion==2) {
			saldo = ingresar(saldo);
			System.out.println("Su nuevo saldo es " + saldo);
			break;
		}
		case 3:if(opcion==3) {
			System.out.println("Pon un numero para obtener el primo");
			num = in.nextInt();
			boolean primo = esprimo(num);
			if (primo == true) {
				System.out.println(num + " es primo");
			} else {
				System.out.println(num + " no es primo");
			}
			break;}
		case 4:if(opcion==4) {
			System.out.println("Pon un numero para obtener el factorial");
			n = in.nextInt();
			factorial=cfactorial(n);
			System.out.println("El factorial es "+factorial);
			break;}
		default:
			System.out.println("Error ");
		}
			in.nextLine();
		System.out.println("¿Quieres realizar otra operacion (si para hacerlo)");
		respuesta=in.nextLine();
		}while(respuesta.equalsIgnoreCase("si"));
	
	}

	static int mirarsaldo(int saldo) {
		Scanner in = new Scanner(System.in);
		System.out.println("Tu saldo actual es de " + saldo);
		return saldo;
	}

	static int ingresar(int saldo) {
		Scanner in = new Scanner(System.in);
		System.out.println("Cuanto dinero quieres ingresar");
		int dinero;
		dinero = in.nextInt();
		System.out.println("Ha ingresado " + dinero + " a tu cuenta");
		saldo = saldo + dinero;
		return saldo;
	}

	static boolean esprimo(int num) {
		for (int primo = 2; primo <= (num - 1); primo++) {
			if (primo % 2 == 0) {
				return false;
			}
		}
		return true;
	}

	static int cfactorial(int n) {
		int factorial = 1;
		for (int f = 1; f <= n; f++) {
			factorial = factorial * f;
		}
		return factorial;
	}

}
