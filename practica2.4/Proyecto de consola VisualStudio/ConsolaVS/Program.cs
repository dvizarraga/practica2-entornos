﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsolaVS
{
    class Program
    {
        static void Main(string[] args)
        {
            int  saldo = 500, n = 0, num=0, factorial, opcion=0;
            String respuesta, opcion1,numprimo,numfactorial;
            do
            {
                do
                {
                    System.Console.WriteLine("Elige una opcion (1-4)");
                    System.Console.WriteLine("1 Mirar tu saldo");
                    System.Console.WriteLine("2 Ingresar dinero");
                    System.Console.WriteLine("3 Comprobar numero primo");
                    System.Console.WriteLine("4 Obterner el factorial de un numero");
                    opcion1 = System.Console.ReadLine();
                    opcion = int.Parse(opcion1);
                    if (opcion > 4)

                    {
                        System.Console.WriteLine("Error, pon un numero del 1 al 4 ");
                    }
                } while (opcion > 4);

                switch (opcion)
                {
                    case 1:
                        if (opcion == 1)
                        {
                            saldo = mirarsaldo(saldo);  
                        }
                        break;
                    case 2:
                        
                            saldo = ingresar(saldo);
                            System.Console.WriteLine("Su nuevo saldo es " + saldo);
                        

                        break;
                        
                    case 3:
                       
                            System.Console.WriteLine("Pon un numero para obtener el primo");
                        numprimo = System.Console.ReadLine();
                            num =int.Parse(numprimo);
                            Boolean primo = esprimo(num);
                            if (primo == true)
                            {
                                System.Console.WriteLine(num + " es primo");
                            }
                            else
                            {
                                System.Console.WriteLine(num + " no es primo");
                            }
                        
                        break;
                        
                    case 4:
                        
                            System.Console.WriteLine("Pon un numero para obtener el factorial");
                            numfactorial= System.Console.ReadLine();
                            factorial = int.Parse(numfactorial);
                            factorial = cfactorial(n);
                            System.Console.WriteLine("El factorial es " + factorial);
                        
                        break;
                        
                    default:
                        System.Console.WriteLine("Error ");

                        break;
                }
               
                System.Console.WriteLine("¿Quieres realizar otra operacion (si para hacerlo)");
                respuesta = System.Console.ReadLine();
            } while (respuesta.Equals("si"));
        }
        static int mirarsaldo(int saldo)
        {
            
            System.Console.WriteLine("Tu saldo actual es de " + saldo);
            return saldo;
        }

        static int ingresar(int saldo)
        {
            
            System.Console.WriteLine("Cuanto dinero quieres ingresar");
            int dinero;
            string dineroString; 
              dineroString=System.Console.ReadLine();
            dinero = int.Parse(dineroString);
            System.Console.WriteLine("Ha ingresado " + dinero + " a tu cuenta");
            saldo = saldo + dinero;
            return saldo;
        }

        static bool esprimo(int num)
        {
            for (int primo = 2; primo <= (num - 1); primo++)
            {
                if (primo % 2 == 0)
                {
                    return false;
                }
            }
            return true;
        }

        static int cfactorial(int n)
        {
            int factorial = 1;
            for (int f = 1; f <= n; f++)
            {
                factorial = factorial * f;
            }
            return factorial;
        }
    }
}
